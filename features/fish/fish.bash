#!/usr/bin/env bash

apt-add-repository ppa:fish-shell/release-2 -y
apt update
apt install -y fish

chsh -s `which fish`

fish -c "set -U fish_user_paths /snap/bin $fish_user_paths"

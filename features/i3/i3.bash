#!/usr/bin/env bash

apt install -y i3 feh compton ffmpeg scrot rofi

# i3 Config
mkdir -p $HOME/.config/i3
cp ./resources/config $HOME/.config/i3/config

#i3 Status
mkdir -p $HOME/.config/i3status
cp ./resources/status.conf $HOME/.config/i3status/config

# i3 Lock
mkdir -p $HOME/.desktop/tasks
cp ./resources/i3-lock.bash $HOME/.desktop/tasks/i3-lock.bash

# i3 Lock Service
cp ./resources/i3-lock.service /etc/systemd/system/i3-lock.service
systemctl enable i3-lock.service

# i3 Wallpapers
mkdir -p $HOME/.desktop/wallpapers
cp ./resources/wallpapers/* $HOME/.desktop/wallpapers

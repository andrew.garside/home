#!/usr/bin/env bash

mkdir -p ~/.local/share/fonts

for type in Bold Light Medium Regular Retina; do
    wget -O ~/.local/share/fonts/FiraCode-${type}.ttf \
    "https://github.com/tonsky/FiraCode/blob/master/distr/ttf/FiraCode-${type}.ttf?raw=true";
done

for type in Bold Regular Medium; do
    wget -O ~/.local/share/fonts/FiraMono-${type}.ttf \
    "https://github.com/mozilla/Fira/blob/master/ttf/FiraMono-${type}.ttf?raw=true";
done

for type in Bold BoldItalic Book BookItalic Eight EightItalic ExtraBold ExtraBoldItalic ExtraLight ExtraLightItalic Four FourItalic Hair HairItalic Heavy HeavyItalic Italic Light LightItalic Medium MediumItalic Regular SemiBold SemiBoldItalic Thin ThinItalic Two TwoItalic Ultra UltraItalic UltraLight UltraLightItalic; do
    wget -O ~/.local/share/fonts/FiraSans-${type}.ttf \
    "https://github.com/mozilla/Fira/blob/master/ttf/FiraSans-${type}.ttf?raw=true";
done

fc-cache -f

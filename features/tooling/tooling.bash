#!/usr/bin/env bash

apt install -y htop screenfetch steam lxappearance gtk-chtheme qt4-qtconfig

snap install phpstorm --classic
# snap install datagrip --classic
snap install vscode --classic
snap install slack --classic
snap install spotify
# snap install libreoffice
snap install discord

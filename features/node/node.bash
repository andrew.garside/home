#!/usr/bin/env bash

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
apt install -y nodejs

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
apt install -y yarn

fish -c "set -U fish_user_paths ~/.yarn/bin $fish_user_paths"

wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

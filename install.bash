#!/usr/bin/env bash

apt update
apt upgrade -y

apt install -y git curl

pushd ./features/sddm
  bash ./sddm.bash
popd

pushd ./features/fish
  bash ./fish.bash
popd

pushd ./features/fonts
  bash ./fonts.bash
popd

pushd ./features/i3
  bash ./i3.bash
popd

pushd ./features/node
  bash ./node.bash
popd

pushd ./features/tooling
  bash ./tooling.bash
popd

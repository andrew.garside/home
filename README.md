# Home

## Installation

### Test dependencies

```sh
wget https://gitlab.com/andrew.garside/home/-/archive/master/home-master.zip && 
unzip home-master.zip && rm -rf home-master.zip  && pushd home-master && bash -e test.bash && popd && rm -rf home-master
```

### Do it live!

```sh
wget https://gitlab.com/andrew.garside/home/-/archive/master/home-master.zip && 
unzip home-master.zip && rm -rf home-master.zip  && pushd home-master && sudo bash -e install.bash && popd && home-master
```
